import React, {Component} from 'react';
import './index.css';

export default class FieldBirthDate extends Component {
    constructor(props) {
        super(props);
        this.state = {birthDate: ""};

        this.onFormChange = this.onFormChange.bind(this);
    }

    async onFormChange(event){

        await this.setState({
            birthDate: event.target.value,
        });

        await this.props.updateFieldBirthDate(this.state.birthDate);
    }

    render(){
        return <div className="field-item">
            <label>
                Дата народження:
                <input name="birthDate"
                       type="date"
                       onChange={this.onFormChange}
                       value={this.birthDate}/>
            </label>
        </div>
    };
}