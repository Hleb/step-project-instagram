import React, {Component} from 'react';
import './index.css';

export default class FieldPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {password: ""};

        this.onFormChange = this.onFormChange.bind(this);
    }

    async onFormChange(event){

            await this.setState({
                password: event.target.value,
            });

            await this.props.updateFieldPassword(this.state.password);
    }

    render(){
        return <div className="field-item">
            <label>
                Пароль:
                <input name="password"
                       type="password"
                       onChange={this.onFormChange}
                       value={this.password}/>
            </label>
        </div>
    };
}