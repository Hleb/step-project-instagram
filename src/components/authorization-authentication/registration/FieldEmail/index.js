import React, {Component} from 'react';
import './index.css';

export default class FieldEmail extends Component {
    constructor(props) {
        super(props);
        this.state = {email: ""};

        this.onFormChange = this.onFormChange.bind(this);
    }

    async onFormChange(event){

        await this.setState({
            email: event.target.value,
        });

        await this.props.updateFieldEmail(this.state.email);
    }

    render(){
        return <div className="field-item">
            <label>
                E-mail:
                <input name="email"
                       type="email"
                       onChange={this.onFormChange}
                       value={this.email}/>
            </label>
        </div>
    };
}