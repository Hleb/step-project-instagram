import React, {Component} from 'react';
import './index.css';

export default class FieldLastName extends Component {
    constructor(props) {
        super(props);
        this.state = {lastName: ""};

        this.onFormChange = this.onFormChange.bind(this);
    }

    async onFormChange(event){

        await this.setState({
            lastName: event.target.value,
        });

        await this.props.updateFieldLastName(this.state.lastName);
    }

    render(){
        return <div className="field-item">
            <label>
                Прізвище:
                <input name="lastName"
                       type="text"
                       onChange={this.onFormChange}
                       value={this.lastName}/>
            </label>
        </div>
    };
}