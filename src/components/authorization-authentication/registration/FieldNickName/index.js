import React, {Component} from 'react';
import './index.css';

export default class FieldNickName extends Component {
    constructor(props) {
        super(props);
        this.state = {nickName: ""};

        this.onFormChange = this.onFormChange.bind(this);
    }

    async onFormChange(event){

        await this.setState({
            nickName: event.target.value,
        });

        await this.props.updateFieldNickName(this.state.nickName);
    }

    render(){
        return <div className="field-item">
            <label>
                Нікнейм:
                <input name="nickName"
                       type="text"
                       onChange={this.onFormChange}
                       value={this.nickName}/>
            </label>
        </div>
    };
}