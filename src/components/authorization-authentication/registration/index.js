import React, {Component} from 'react';
import './index.css';
import FieldFirstName from './FieldFirstName';
import FieldLastName from './FieldLastName';
import FieldNickName from './FieldNickName';
import FieldBirthDate from './FieldBirthDate';
import FieldEmail from './FieldEmail';
import FieldPassword from './FieldPassword';

export default class Registration extends Component {
    constructor(props) {
        super(props);
            this.state = {
                firstName: "",
                lastName: "",
                nickName: "",
                birthDate: "",
                email: "",
                password: "",
            };
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.updateFieldFirstName = this.updateFieldFirstName.bind(this);
    }

    updateFieldFirstName = (value) => {
        this.setState({firstName: value});
    };

    updateFieldLastName = (value) => {
        this.setState({lastName: value});
    };

    updateFieldNickName = (value) => {
        this.setState({nickName: value});
    };

    updateFieldBirthDate = (value) => {
        this.setState({birthDate: value});
    };

    updateFieldEmail = (value) => {
        this.setState({email: value});
    };

    updateFieldPassword = (value) => {
        this.setState({password: value});
    };

    async onFormSubmit (event) {
        event.preventDefault();

        let userData = this.state;

        await fetch("https://inst-server.herokuapp.com/auth/signup",{
            method: "POST",
            headers: {
                "Content-Type": "application/json;charset=utf-8",
                "Accept": "application/json",
                "Access-Control-Allow-Origin": "https://inst-server.herokuapp.com",
                "X-Requested-With": "XMLHttpRequest",
                "Accept-Control-Allow-Methods": "GET, POST, PUT, DEL, OPTIONS",
                "Accept-Control-Allow-Headers": "Content-Type, Accept-Control-Allow-Methods, Authorization, X-Requested-With"
            },
            body: JSON.stringify(userData),
        });
    }

    render(){
        return <div className="form-wrapper">
            <form className="registration-form"
                  onSubmit={this.onFormSubmit}>

                <FieldFirstName updateFieldFirstName={this.updateFieldFirstName}/>
                <FieldLastName updateFieldLastName={this.updateFieldLastName}/>
                <FieldNickName updateFieldNickName={this.updateFieldNickName}/>
                <FieldBirthDate updateFieldBirthDate={this.updateFieldBirthDate}/>
                <FieldEmail updateFieldEmail={this.updateFieldEmail}/>
                <FieldPassword updateFieldPassword={this.updateFieldPassword}/>

                <div className="field-item">
                    <button onClick={this.onFormSubmit}>Зареєструватися</button>
                </div>

            </form>
        </div>
    };
}