import React, {Component} from 'react';
import './index.css';

export default class FieldFirstName extends Component {
    constructor(props) {
        super(props);
        this.state = {firstName: ""};

        this.onFormChange = this.onFormChange.bind(this);
    }

    async onFormChange(event){

        await this.setState({
            firstName: event.target.value,
        });

        await this.props.updateFieldFirstName(this.state.firstName);
    }

    render(){
        return <div className="field-item">
            <label>
                Ім'я:
                <input name="firstName"
                       type="text"
                       onChange={this.onFormChange}
                       value={this.firstName}
                />
            </label>
        </div>
    };
}